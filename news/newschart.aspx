﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="newschart.aspx.cs" Inherits="StockMarket.news.newschart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2>News Chart</h2>
    <asp:GridView ID="newsGrid" runat="server" CellPadding="4" ForeColor="#333333" 
        GridLines="None" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Date" HeaderText="Date" 
                DataFormatString="{0:d}" />
            <asp:BoundField DataField="Currency" HeaderText="Currency" />
            <asp:BoundField DataField="Sunday" HeaderText="Sunday" />
            <asp:BoundField DataField="Monday" HeaderText="Monday" />
            <asp:BoundField DataField="Tuesday" HeaderText="Tuesday" />
            <asp:BoundField DataField="Wednesday" HeaderText="Wednesday" />
            <asp:BoundField DataField="Thursday" HeaderText="Thursday" />
            <asp:BoundField DataField="Friday" HeaderText="Friday" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
</asp:Content>
