﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="StockMarket.news._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2>Latest News</h2>
    <asp:GridView ID="newsGrid" runat="server" CellPadding="4" ForeColor="#333333" 
        GridLines="None" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Weekdate" HeaderText="Weekdate" 
                DataFormatString="{0:d}" SortExpression="Date" />
            <asp:BoundField DataField="Date" HeaderText="Date" />
            <asp:BoundField DataField="Time" HeaderText="Time" />
            <asp:BoundField DataField="TimeZone" HeaderText="Time Zone" />
            <asp:BoundField DataField="Currency" HeaderText="Currency" />
            <asp:BoundField DataField="NewsEvent" HeaderText="Event" />
            <asp:BoundField DataField="Importance" HeaderText="Importance" />
            <asp:BoundField DataField="Actual" HeaderText="Actual" />
            <asp:BoundField DataField="Forecast" HeaderText="Forecast" />
            <asp:BoundField DataField="Previous" HeaderText="Previous" />
            
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
</asp:Content>
