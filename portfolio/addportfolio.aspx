﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="addportfolio.aspx.cs" Inherits="StockMarket.portfolio.addportfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table class="style1">
            <tr>
                <td align="right" class="style2">
                    Company Name :
                </td>
                <td>
                    <asp:DropDownList ID="companyDropDownList" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" class="style2">
                    Sector :</td>
                <td>
                    <asp:DropDownList ID="sectorDropDownList" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" class="style2">
                    Position :</td>
                <td>
                    <asp:DropDownList ID="positionDropDownList" runat="server">
                        <asp:ListItem>long</asp:ListItem>
                        <asp:ListItem>short</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" class="style2">
                    Weight :</td>
                <td>
                    <asp:TextBox ID="weightTextBox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" class="style2">
                    Spy Hedge :</td>
                <td>
                    <asp:TextBox ID="spyhedgeTextBox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" class="style2">
                    Type :</td>
                <td>
                    <asp:DropDownList ID="typeDropDownList" runat="server">
                        <asp:ListItem>Entry</asp:ListItem>
                        <asp:ListItem>Hold</asp:ListItem>
                        <asp:ListItem>Exit</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" class="style2">
                    Status :</td>
                <td>
                    <asp:DropDownList ID="statusDropDownList" runat="server">
                        <asp:ListItem>Open</asp:ListItem>
                        <asp:ListItem>Entered</asp:ListItem>
                        <asp:ListItem>Closed</asp:ListItem>
                        <asp:ListItem>Cancelled</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" class="style2">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="savePortfolioBtn" runat="server" 
                        onclick="savePortfolioBtn_Click" Text="Save" />
                </td>
            </tr>
        </table>
</asp:Content>
