﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="StockMarket.portfolio._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Portfolios</h2>
<a href="addportfolio.aspx">Add Portfolio</a>
        <asp:GridView ID="portfolioGrid" runat="server" 
            onselectedindexchanged="portfolioGrid_SelectedIndexChanged" 
        CellPadding="4" ForeColor="#333333" GridLines="None" 
        EmptyDataText="No Data Return" AutoGenerateColumns="False">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:ButtonField CommandName="Select" Text="Edit" />
                <asp:BoundField DataField="CompanyName" HeaderText="Company" />
                <asp:BoundField DataField="SectorName" HeaderText="Sector" />
                <asp:BoundField DataField="Position" HeaderText="Position" />
                <asp:BoundField DataField="Weight" HeaderText="Weight" />
                <asp:BoundField DataField="SpyHedge" HeaderText="Spy Hedge" />
                <asp:BoundField DataField="Type" HeaderText="Type" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>

<fieldset>
    <legend>Open</legend>
    <asp:Button ID="enteredBtn" runat="server" Text="Enter All" 
        onclick="enteredBtn_Click" />
    <asp:GridView ID="openPortfolioGrid" runat="server" CellPadding="4" 
        ForeColor="#333333" GridLines="None" EmptyDataText="No Available Data" 
        AutoGenerateColumns="False" onrowcommand="openPortfolioGrid_RowCommand" DataKeyNames="PortfolioID">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                       <asp:LinkButton ID="enterLinkButton" runat="server" CommandArgument='<%#Eval("PortfolioID")%>' CommandName="Enter"  Text="Enter">
                        </asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="cancelLinkButton" runat="server" CausesValidation="false" 
                            CommandName="Cancel" Text="Cancel" CommandArgument='<%#Eval("PortfolioID")%>'></asp:LinkButton>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PortfolioID" HeaderText="ID" />
                <asp:BoundField DataField="CompanyName" HeaderText="Company" />
                <asp:BoundField DataField="SectorName" HeaderText="Sector" />
                <asp:BoundField DataField="Position" HeaderText="Position" />
                <asp:BoundField DataField="Weight" HeaderText="Weight" />
                <asp:BoundField DataField="SpyHedge" HeaderText="Spy Hedge" />
                <asp:BoundField DataField="Type" HeaderText="Type" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
</fieldset>

<fieldset>
    <legend>Entered</legend>
    <asp:Button ID="closedBtn" runat="server" Text="Close All" 
        onclick="closedBtn_Click" />
    <asp:GridView ID="enteredPortfolioGrid" runat="server" CellPadding="4" 
        ForeColor="#333333" GridLines="None" EmptyDataText="No Available Data"
        AutoGenerateColumns="False" onrowcommand="enteredPortfolioGrid_RowCommand"  DataKeyNames="PortfolioID">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false"  Text="Close" CommandArgument='<%#Eval("PortfolioID")%>' CommandName="Close"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CompanyName" HeaderText="Company" />
                <asp:BoundField DataField="SectorName" HeaderText="Sector" />
                <asp:BoundField DataField="Position" HeaderText="Position" />
                <asp:BoundField DataField="Weight" HeaderText="Weight" />
                <asp:BoundField DataField="SpyHedge" HeaderText="Spy Hedge" />
                <asp:BoundField DataField="Type" HeaderText="Type" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:BoundField DataField="EnteredOn" HeaderText="Entered Date" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
</fieldset>

<fieldset>
    <legend>Closed</legend>
    <asp:GridView ID="closedPortfolioGrid" runat="server" CellPadding="4" 
        ForeColor="#333333" GridLines="None" EmptyDataText="No Available Data" 
        AutoGenerateColumns="False">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775"/>
            <Columns>
                <asp:BoundField DataField="CompanyName" HeaderText="Company" />
                <asp:BoundField DataField="SectorName" HeaderText="Sector" />
                <asp:BoundField DataField="Position" HeaderText="Position" />
                <asp:BoundField DataField="Weight" HeaderText="Weight" />
                <asp:BoundField DataField="SpyHedge" HeaderText="Spy Hedge" />
                <asp:BoundField DataField="Type" HeaderText="Type" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:BoundField DataField="ClosedOn" HeaderText="Closed Date" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
</fieldset>
</asp:Content>
